const mongoose = require("mongoose");
const Schema = mongoose.Schema;

module.exports = function UsersModel() {
  const userSchema = new Schema({
    nama: String,
    hobi: String,
    alamat: String,
    no_telp: Number,
  });

  return mongoose.model("users", userSchema);
};
