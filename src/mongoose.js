const mongoose = require("mongoose");

module.exports = async function mongoConnect() {
  await mongoose.connect("mongodb+srv://admin:admin123@mgiandrea.cxyf5.mongodb.net/mgi_andrea?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });
};
